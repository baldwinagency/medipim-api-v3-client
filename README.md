This library helps you connect to and make use of the Medipim API (v3).

You aren't required to use this library to call the API, but if you are using php it should be helpful.

* Medipim platform: https://platform.medipim.be
* Medipim API v3 documentation: https://platform.medipim.be/docs/api/v3/
* Support: support@medipim.be

## Features ##

* Handles authentication, just provide your API key and secret.
* Takes care of encoding requests and decoding results.
* Throttles your requests and automatically retries a request should you exceed the maximum request rate.
* Neatly translates errors into exceptions.

## Installation ##

Using composer:

```
$ composer require baldwin/medipim-api-v3-client
```

## Basic usage ##

Instantiate the client class with your API credentials. Use `get`, `post` or `stream` to make requests.

```
<?php

use Medipim\Api\V3\MedipimApiV3Client;

require "vendor/autoload.php";

$apiKeyId = ...;
$apiKeySecret = ...;
$client = new MedipimApiV3Client($apiKeyId, $apiKeySecret);

$products = $client->post("/v3/products/search", [
    "filter" => ["updatedSince" => time() - 24*60*60]
]);

foreach ($products["results"] as $product) {
    // ...
}
```

## API ##

### MedipimApiV3Client ###

#### __construct(int $apiKeyId, string $apiKeySecret)

Creates a new client.

#### get(string $path, array $query): array

Performs a GET request.

```
<?php
$r = $client->get("/v3/apb-categories/all");
var_dump($r); // array(2) { ["meta"] => ..., ["results"] => ... }
```

#### post(string $path, array $body): array|\Iterator

Performs a simple POST request with a json body.

Note that a few POST endpoints stream their response, in that case this function returns an `\Iterator` over the results.

```
<?php
$r = $client->post("/v3/products/search", ["filter" => ["updatedSince" => strtotime("yesterday")]]);
var_dump($r); // array(2) { ["meta"] => ..., ["results"] => ... }
```

#### stream($path, array $query = [], callable $callback)

This method should be used only for the /export endpoint.

A callable should be defined to handle the data.

#### setBaseUrl(string $url): void

By default the client connects to 'https://api.medipim.be'.
If you want to use the client with our sandbox server you can do so using this method.

```
<?php
$client->setBaseUrl("https://api.sandbox.medipim.be");
```

Note that sandbox credentials are separate from our main server. Sandbox product data is wiped regularly.

### MedipimApiV3Error ###

extends `\RuntimeException`.

API errors are converted to an instance of this class and thrown.

You can use this object like an array to access any properties of the error.

```
<?php
try {
    $r = $client->get("/v3/foo/bar");
    // responds with {"error": {"code": "endpoint_not_found", ...}}
} catch (MedipimApiV3Error $e) {
    var_dump($e["code"]); // string(18) "endpoint_not_found"
}
```
